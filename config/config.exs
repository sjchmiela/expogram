# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :expogram, ecto_repos: [Expogram.Repo]

# Configures the endpoint
config :expogram, ExpogramWeb.Endpoint,
  secret_key_base: "h56Q8E/LTYrQV9dHpKaFkCbPOR6lnM13muZyG9Nf1U2wCS5Ai3MMFUog7JPmtiJW",
  render_errors: [view: ExpogramWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: Expogram.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
