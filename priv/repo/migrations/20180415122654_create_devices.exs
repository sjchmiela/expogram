defmodule Expogram.Repo.Migrations.CreateDevices do
  use Ecto.Migration

  def change do
    create table(:devices) do
      add :identifier, :string
      add :name, :string
      add :platform, :string
      add :token, :string

      timestamps()
    end

  end
end
