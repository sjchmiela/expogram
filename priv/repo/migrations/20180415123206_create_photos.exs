defmodule Expogram.Repo.Migrations.CreatePhotos do
  use Ecto.Migration

  def change do
    create table(:photos) do
      add :uuid, :string
      add :image, :string
      add :description, :string
      add :device_id, references(:devices, on_delete: :nothing)

      timestamps()
    end

    create index(:photos, [:device_id])
  end
end
