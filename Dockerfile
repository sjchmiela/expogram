# Source: https://pspdfkit.com/blog/2018/how-to-run-your-phoenix-application-with-docker/

# ./Dockerfile

# Extend from the official Elixir image
FROM elixir:1.6.4

RUN apt-get update && \
  apt-get install -y postgresql-client

# Install hex package manager
RUN mix local.hex --force
RUN mix local.rebar --force

# Create app directory and copy the Elixir projects into it
RUN mkdir /app
COPY . /app
WORKDIR /app

RUN mix deps.get

# Compile the project
RUN mix do compile

RUN chmod +x /app/entrypoint.sh

CMD ["/app/entrypoint.sh"]
