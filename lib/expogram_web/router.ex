defmodule ExpogramWeb.Router do
  use ExpogramWeb, :router

  pipeline :api do
    plug(:accepts, ["json"])
  end

  scope "/api", ExpogramWeb do
    pipe_through(:api)
    
    resources "/photos", PhotoController, only: [:create, :index]
  end
end
