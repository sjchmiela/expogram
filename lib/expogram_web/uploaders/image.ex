defmodule ExpogramWeb.Uploaders.Image do
  use Arc.Definition
  def __storage, do: Arc.Storage.Local

  # Include ecto support (requires package arc_ecto installed):
  use Arc.Ecto.Definition

  @versions [:original]

  # To add a thumbnail version:
  # @versions [:original, :thumb]

  # Whitelist file extensions:
  def validate({file, _}) do
    ~w(.jpg .jpeg) |> Enum.member?(Path.extname(file.file_name))
  end

  # Define a thumbnail transformation:
  def transform(:original, _) do
    {:convert, "-strip -thumbnail 1000x1000^ -gravity center -extent 1000x1000 -auto-orient -format jpg", :jpg}
  end

  def filename(_, {_, scope}) do
    "photo_#{scope.uuid}"
  end

  # Override the storage directory:
  def storage_dir(_, {_, scope}) do
    "uploads/images/#{scope.device.id}/"
  end

  # Provide a default URL if there hasn't been a file uploaded
  # def default_url(version, scope) do
  #   "/images/avatars/default_#{version}.png"
  # end

  # Specify custom headers for s3 objects
  # Available options are [:cache_control, :content_disposition,
  #    :content_encoding, :content_length, :content_type,
  #    :expect, :expires, :storage_class, :website_redirect_location]
  #
  # def s3_object_headers(version, {file, scope}) do
  #   [content_type: Plug.MIME.path(file.file_name)]
  # end
end
