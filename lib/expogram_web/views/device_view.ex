defmodule ExpogramWeb.DeviceView do
  use ExpogramWeb, :view
  alias ExpogramWeb.DeviceView

  def render("index.json", %{devices: devices}) do
    %{data: render_many(devices, DeviceView, "device.json")}
  end

  def render("show.json", %{device: device}) do
    %{data: render_one(device, DeviceView, "device.json")}
  end

  def render("device.json", %{device: device}) do
    %{identifier: device.identifier,
      platform: device.platform,
      name: device.name}
  end
end
