defmodule ExpogramWeb.PhotoView do
  use ExpogramWeb, :view
  alias ExpogramWeb.PhotoView

  def render("index.json", %{photos: photos, base_url: base_url}) do
    %{data: render_many(photos, PhotoView, "photo.json", base_url: base_url)}
  end

  def render("show.json", %{photo: photo, base_url: base_url}) do
    %{data: render_one(photo, PhotoView, "photo.json", base_url: base_url)}
  end

  def render("photo.json", %{photo: photo, base_url: base_url}) do
    %{id: Expogram.Photos.cursor_from_photo(photo),
      image_url: base_url <> ExpogramWeb.Uploaders.Image.url({photo.image, photo}, signed: true),
      description: photo.description,
      inserted_at: photo.inserted_at |> NaiveDateTime.to_iso8601,
      device: ExpogramWeb.DeviceView.render("device.json", %{device: photo.device})
    }
  end
end
