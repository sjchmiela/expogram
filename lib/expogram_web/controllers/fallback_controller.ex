defmodule ExpogramWeb.FallbackController do
  @moduledoc """
  Translates controller action results into valid `Plug.Conn` responses.

  See `Phoenix.Controller.action_fallback/1` for more details.
  """
  use ExpogramWeb, :controller

  def call(conn, {:error, %Ecto.Changeset{} = changeset}) do
    IO.inspect(changeset)
    conn
    |> put_status(:unprocessable_entity)
    |> render(ExpogramWeb.ChangesetView, "error.json", changeset: changeset)
  end

  def call(conn, {:error, reason}) when is_bitstring(reason) do
    IO.inspect(reason)
    conn
    |> put_status(:unprocessable_entity)
    |> render(ExpogramWeb.ErrorView, "error.json", reason: reason)
  end

  def call(conn, {:error, :not_found}) do
    conn
    |> put_status(:not_found)
    |> render(ExpogramWeb.ErrorView, :"404")
  end
end
