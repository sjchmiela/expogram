defmodule ExpogramWeb.PhotoController do
  use ExpogramWeb, :controller

  alias Expogram.Photos
  alias Expogram.Photos.Photo
  
  alias Expogram.Accounts
  alias Expogram.Accounts.Device

  action_fallback ExpogramWeb.FallbackController

  def index(conn, params) do
    with {:ok, photos} <- Photos.list_photos(params) do
      render(conn, "index.json", photos: photos, base_url: base_url(conn))
    end
  end

  def create(conn, %{"photo" => photo_params, "device" => device_params}) do
    with {:ok, %Device{} = device} <- Accounts.find_or_create_device(device_params),
      {:ok, %Device{} = device} <- Accounts.update_device(device, device_params),
      {:ok, %Photo{} = photo} <- Photos.create_photo(device, photo_params) do
      conn
      |> put_status(:created)
      |> render("show.json", photo: photo, base_url: base_url(conn))
    end
  end

  defp base_url(conn) do
    [host] = get_req_header(conn, "host")
    "#{conn.scheme}://#{host}"
  end
end
