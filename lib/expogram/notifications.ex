defmodule Expogram.Notifications do
  use Tesla

  plug Tesla.Middleware.BaseUrl, "https://exp.host/--/api/v2/push/send"
  plug Tesla.Middleware.Compression, format: "gzip"
  plug Tesla.Middleware.Headers, [
    {"accept", "application.json"}
  ]
  plug Tesla.Middleware.JSON

  def notify(devices, title: title, body: body) do
    json = Enum.map(devices, fn device -> %{
      to: device.token,
      sound: "default",
      body: body,
      title: title
    } end)
    post("push/send", json)
  end
end
