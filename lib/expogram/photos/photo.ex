defmodule Expogram.Photos.Photo do
  use Ecto.Schema
  use Arc.Ecto.Schema
  import Ecto.Changeset


  schema "photos" do
    field :description, :string, default: ""
    field :uuid, :string
    field :image, ExpogramWeb.Uploaders.Image.Type
    belongs_to(:device, Expogram.Accounts.Device)

    timestamps()
  end

  @doc false
  def changeset(photo, attrs) do
    photo
    |> Map.update(:uuid, Ecto.UUID.generate, fn val -> val || Ecto.UUID.generate end)
    |> cast(attrs, [:description, :device_id])
    |> cast_attachments(attrs, [:image])
    |> validate_required([:image, :uuid])
    |> validate_not_nil([:description])
    |> cast_assoc(:device)
    |> assoc_constraint(:device)
  end

  def validate_not_nil(changeset, fields) do
    Enum.reduce(fields, changeset, fn field, changeset ->
      if get_field(changeset, field) == nil do
        add_error(changeset, field, "nil")
      else
        changeset
      end
    end)
  end
end
