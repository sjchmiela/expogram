defmodule Expogram.Photos do
  import Ecto.Query, warn: false
  alias Expogram.Repo

  alias Expogram.Photos.Photo

  def list_photos(params) do
    with photos_query <- from(p in Photo),
      {:ok, directed_query} <- direction_clause(photos_query, params),
      {:ok, sliced_query} <- slice_clause(directed_query, params),
      {:ok, paginated_query} <- limit_clause(sliced_query, params) do
        {:ok, paginated_query |> preload(:device) |> Repo.all}
      end
  end

  
  defp direction_clause(queryable, %{"last" => _last}) do
    {:ok, order_by(queryable, [desc: :id])}
  end

  defp direction_clause(queryable, _) do
    {:ok, order_by(queryable, [asc: :id])}
  end

  defp slice_clause(queryable, %{"before" => last}) do
    with {:ok, decoded_id} <- photo_id_from_cursor(last) do
      {:ok, where(queryable, [p], p.id < ^decoded_id)}
    end
  end

  defp slice_clause(queryable, %{"after" => first}) do
    with {:ok, decoded_id} <- photo_id_from_cursor(first) do
      {:ok, where(queryable, [p], p.id > ^decoded_id)}
    end
  end

  defp slice_clause(queryable, _) do
    {:ok, queryable}
  end

  defp limit_clause(queryable, %{"first" => first}) do
    do_limit_clause(queryable, first)
  end

  defp limit_clause(queryable, %{"last" => last}) do
    do_limit_clause(queryable, last)
  end

  defp limit_clause(queryable, _) do
    {:ok, limit(queryable, 10)}
  end

  defp do_limit_clause(queryable, limit_string) do
    case Integer.parse(limit_string) do
      {value, ""} when value > 0 -> {:ok, limit(queryable, ^value)}
      {_value, ""} -> {:error, "Limit must be positive"}
      :error -> {:error, "Invalid limit provided"}
    end
  end

  def cursor_from_photo(photo) do
    Base.encode64("#{photo.id}")
  end

  defp photo_id_from_cursor(cursor) do
    with {:ok, value} <- Base.decode64(cursor),
      {id, ""} <- Integer.parse(value) do
        {:ok, id}
      else
        :error -> {:error, "Invalid cursor provided"}
      end
  end

  def create_photo(device, attrs \\ %{}) do
    %Photo{device: device}
    |> Photo.changeset(Map.update(attrs, "description", "", fn val -> val || "" end))
    |> Repo.insert()
  end
end
