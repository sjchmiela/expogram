defmodule Expogram.Accounts do
  import Ecto.Query, warn: false
  alias Expogram.Repo

  alias Expogram.Accounts.Device

  def find_device_by_identifier(identifier),
    do: Repo.get_by(Device, [identifier: identifier])

  def create_device(attrs) do
    %Device{}
    |> Device.changeset(attrs)
    |> Repo.insert()
  end

  def find_or_create_device(%{"identifier" => identifier} = params) do
    case find_device_by_identifier(identifier) do
      nil -> create_device(params)
      device -> {:ok, device}
    end
  end

  def find_or_create_device(_) do
    {:error, "Invalid device params provided - no device identifier"}
  end

  def update_device(device, params) do
    device
    |> Device.changeset(params)
    |> Repo.update()
  end
end
