defmodule Expogram.Accounts.Device do
  use Ecto.Schema
  import Ecto.Changeset


  schema "devices" do
    field :identifier, :string
    field :name, :string
    field :token, :string
    field :platform, :string

    timestamps()
  end

  @doc false
  def changeset(device, attrs) do
    device
    |> cast(attrs, [:identifier, :name, :token, :platform])
    |> validate_inclusion(:platform, ["ios", "android"])
    |> validate_required([:identifier])
  end
end
